//
//  logController.swift
//  Jue
//
//  Created by Alberto Cesare Barbon on 11/04/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class logController: UIViewController, FBSDKLoginButtonDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set("già", forKey: "time")

        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.center = self.view.center
        loginButton.delegate = self
        loginButton.frame = CGRect(x: view.frame.width / 2 - 100, y: view.frame.height - 100, width: 200, height: 40)
        loginButton.layer.cornerRadius = 20
        self.view.addSubview(loginButton)
        
        if (FBSDKAccessToken.current() == nil) {
            //non loggato
            //print(FBSDKAccessToken.current())
            
        } else {
            
            //loggato
            print(FBSDKAccessToken.current())
            perform(#selector(logController.seg), with: nil, afterDelay: 0.00001)
        }
    }
    
    func seg() {
        performSegue(withIdentifier: "segueLogin", sender: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        //performSegue(withIdentifier: "segueLogin", sender: nil)
        
        if (FBSDKAccessToken.current() == nil) {
            //non loggato
            //print(FBSDKAccessToken.current())
            
        } else {
            
            //loggato
            print(FBSDKAccessToken.current())
            performSegue(withIdentifier: "segueLogin", sender: nil)
        }
        
        
        if (error == nil) {
            print("Utente connesso")
            
            
        } else
        {
            print(error.localizedDescription)
            print("annullato")
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
        print("ciao")
        
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        
        print("ciaolog")
        
        return true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
