//
//  ViewController.swift
//  Jue
//
//  Created by Alberto Cesare Barbon on 08/04/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Foundation
import MBCircularProgressBar
import AVFoundation
import Parse
import UIView_draggable
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AudioToolbox

class ViewController: UIViewController, AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var navigationBar: UINavigationBar! = UINavigationBar()
    @IBOutlet weak var imgOverlay: UIImageView!
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var btnPick: UIButton!
    @IBOutlet weak var hashtag: UIView!
    @IBOutlet weak var btSettings : UIButton!
    @IBOutlet weak var btHeart : UIButton!
    
    let imageView = UIImageView()
    
    @IBOutlet weak var labelHash: UILabel!
    
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCaptureStillImageOutput()
    var previewLayer : AVCaptureVideoPreviewLayer?
    let buttonPic : UIButton = UIButton()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    
    var progress : MBCircularProgressBarView = MBCircularProgressBarView()
    var secondi :Int = 0
    var centesimiDisecondo :Int = 0
    var timer = Timer()
    var a = 1
    var foto : UIImage = UIImage()
    
    var arrayHas : [PFObject] = []
    
    var imagePicker: UIImagePickerController = UIImagePickerController()
    
    var arrayScritte : [String] = []
    
    var on : Bool = false
    
    var ofTheDay : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        on = false
        btnPick.setImage(UIImage(named:"FlashOFF.png"), for: .normal)
        
        /*if (on == true) {
            btnPick.setImage(UIImage(named:"FlashON.png"), for: .normal)
        } else {
            btnPick.setImage(UIImage(named:"flashOFF.png"), for: .normal)
        }
        */
        btnPick.setImage(UIImage(named:"flashOFF.png"), for: .normal)
        btnPick.isHidden = false
        view.bringSubview(toFront: btnPick)
        
        
        hashtag.enableDragging()
        hashtag.shouldMoveAlongX = false
        hashtag.shouldMoveAlongY = true
        hashtag.cagingArea = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        imagePicker.delegate = self
        
        
        
        let query = PFQuery(className: "Hashtag")
        query.findObjectsInBackground()
            {(objects, error) in
                
                for object in objects! {
                    
                    self.arrayHas.append(object)
                    
                }
                
                let intero = self.arrayHas.count - 1
                let random = arc4random_uniform(UInt32(intero))
                self.labelHash.text = self.arrayHas[Int(random)]["Hashtag"] as? String
                let userDefaults = Foundation.UserDefaults.standard
                userDefaults.set(self.arrayHas[Int(random)].objectId, forKey: "Hash")
        }
        
        let queryy = PFQuery(className: "Hashtag")
        queryy.whereKey("ofTheDay", equalTo: "otd")
        queryy.findObjectsInBackground()
            {(objects, error) in
                
                for object in objects! {
                    
                    self.ofTheDay = (object["Hashtag"] as? String)!
                    
                }
                
        }
        
        self.becomeFirstResponder()
        UIApplication.shared.isStatusBarHidden = true
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
    
        swap(position: 1)
    
        progress.frame = CGRect(x: view.frame.width / 2 - 60, y: view.frame.height - 150, width: 120, height: 120)
        progress.value = 50
        progress.progressColor = .yellow
        progress.backgroundColor = .clear
        progress.progressLineWidth = 5
        progress.progressStrokeColor = .yellow
        progress.emptyLineColor = .clear
        progress.emptyLineStrokeColor = .clear
        progress.isHidden = true
        view.addSubview(progress)
        
        
        buttonPic.frame = CGRect(x: view.frame.width / 2 - 50, y: view.frame.height - 120, width: 100, height: 100)
        buttonPic.setImage(UIImage(named: "Camera Button.png"), for: .normal)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ViewController.normalTap))
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longTap))
        tapGesture.numberOfTapsRequired = 1
        buttonPic.addGestureRecognizer(tapGesture)
        buttonPic.addGestureRecognizer(longGesture)
        view.addSubview(buttonPic)
        
        view.bringSubview(toFront: hashtag)
        view.bringSubview(toFront: labelHash)
        view.bringSubview(toFront: btnPick)
        view.bringSubview(toFront: btSettings)
        view.bringSubview(toFront: btHeart)
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        on = false
        btnPick.setImage(UIImage(named:"FlashOFF.png"), for: .normal)
        
        labelHash.adjustsFontSizeToFitWidth = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        on = false
        btnPick.setImage(UIImage(named:"FlashOFF.png"), for: .normal)
    }
    
    override var canBecomeFirstResponder: Bool { return true }
    
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        let intero = arrayHas.count - 1
        let random = arc4random_uniform(UInt32(intero))
        labelHash.text = arrayHas[Int(random)]["Hashtag"] as? String
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set(arrayHas[Int(random)].objectId, forKey: "Hash")
        
    }
    
    func seg() {
        
        self.performSegue(withIdentifier: "segueDetail", sender: nil)
        
    }
    
    @IBAction func ofTheday () {
        
        btHeart.setImage(UIImage(named: "HeartColor.png"), for: .normal)
        
        labelHash.text = ofTheDay
        
    }
    
    //Tap gesture
    func normalTap(_ sender: UIGestureRecognizer){
        
        if (on == true) && (a == 0) {
        
        guard let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) else { return }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
        
        }
        
        saveToCamera()
        
        
        
        perform(#selector(ViewController.bg), with: nil, afterDelay: 1)
        
        
        
        
        
        
        
    }
    
    
    
    func bg () {
        
        if (on == true){
        
        guard let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) else { return }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .off
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
            
        }
        
        
        perform(#selector(ViewController.seg), with: nil, afterDelay: 0.5)
    }
    
    func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
            
            timer.invalidate()
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            //Do Whatever You want on Began of Gesture
            
            
            let intervallo :Double = 1
            timer = Timer.scheduledTimer(timeInterval: intervallo, target:self, selector: #selector(ViewController.pic), userInfo: nil, repeats: true)
            print(timer)
        }
        
    }
    
    
    
    @IBAction func cameraPick () {
        
        
        /*imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)*/
        
        /*var device : AVCaptureDevice!
        
        if #available(iOS 10.0, *) {
            let videoDeviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDuoCamera], mediaType: AVMediaTypeVideo, position: .unspecified)!
            let devices = videoDeviceDiscoverySession.devices!
            device = devices.first!
            
        } else {
            // Fallback on earlier versions
            device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        }
        
        if ((device as AnyObject).hasMediaType(AVMediaTypeVideo))
        {
            if (device.hasTorch)
            {
                self.captureSession.beginConfiguration()
                //self.objOverlayView.disableCenterCameraBtn();
                if device.isTorchActive == false {
                    self.flashOn(device: device)
                    self.btnPick.setImage(UIImage(named: "flashON.png"), for: .normal)
                } else {
                    self.flashOff(device: device);
                    self.btnPick.setImage(UIImage(named: "FlashOFF.png"), for: .normal)
                }
                //self.objOverlayView.enableCenterCameraBtn();
                self.captureSession.commitConfiguration()
            }
        }*/
        
        print(a)
        
        if (on == true) && (a == 1) {
            on = false
            btnPick.setImage(UIImage(named:"FlashOFF.png"), for: .normal)
        } else {
            on = true
            btnPick.setImage(UIImage(named:"flashON.png"), for: .normal)
        }
        
    }
    
    private func flashOn(device:AVCaptureDevice)
    {
        do{
            if (device.hasTorch)
            {
                try device.lockForConfiguration()
                device.torchMode = .on
                device.flashMode = .on
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    private func flashOff(device:AVCaptureDevice)
    {
        do{
            if (device.hasTorch){
                try device.lockForConfiguration()
                device.torchMode = .off
                device.flashMode = .off
                device.unlockForConfiguration()
            }
        }catch{
            //DISABEL FLASH BUTTON HERE IF ERROR
            print("Device tourch Flash Error ");
        }
    }
    
    
    /*private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        perform(#selector(ViewController.seg), with: nil, afterDelay: 0.5)
        
        dismiss(animated: true, completion: nil)
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        foto = chosenImage
        
        perform(#selector(ViewController.seg), with: nil, afterDelay: 0.5)
    }*/
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            dismiss(animated: true, completion: nil)
            
            foto = image
            
            perform(#selector(ViewController.seg), with: nil, afterDelay: 1)
        }
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    

    //Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueDetail" {
            let dvc = segue.destination as! detailViewController
            dvc.nuovaFoto = self.foto
            dvc.hasht = labelHash.text
            dvc.frame = hashtag.frame
        }
    }
    
    
    var d = 0
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                
            case UISwipeGestureRecognizerDirection.right:
                
                btHeart.setImage(UIImage(named: "heart.png"), for: .normal)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                
                print(d)
                if (d > 0) {
                    labelHash.text = arrayScritte[d]
                    d = d - 1
                }
                
            case UISwipeGestureRecognizerDirection.left:
                
                btHeart.setImage(UIImage(named: "heart.png"), for: .normal)
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                
                let intero = arrayHas.count - 1
                let random = arc4random_uniform(UInt32(intero))
                labelHash.text = arrayHas[Int(random)]["Hashtag"] as? String
                let userDefaults = Foundation.UserDefaults.standard
                userDefaults.set(arrayHas[Int(random)].objectId, forKey: "Hash")
                
                self.arrayScritte.append(labelHash.text!)
                let countArray = arrayScritte.count
                
                d = countArray - 1
                
                
            default:
                
                break
            }
        }
    }
    
    @IBAction func setting() {
        
        let alert = UIAlertController(title: "Impostazioni", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Logout", style: .default) { action in
            let manager = FBSDKLoginManager()
            manager.logOut()
            
            self.performSegue(withIdentifier: "segueLogout", sender: nil)
        })
        alert.addAction(UIAlertAction(title: "Annulla", style: .cancel) { action in
            //self.dismiss(animated: true, completion: nil)
            
        })
        self.present(alert, animated: true, completion: nil)
        
        
    }


}

