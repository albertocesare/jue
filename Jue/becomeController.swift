//
//  becomeController.swift
//  Jue
//
//  Created by Alberto Cesare Barbon on 11/04/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit

class becomeController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let userDefaults = Foundation.UserDefaults.standard
        let value  = userDefaults.string(forKey: "time")
        
        if (value == nil) {
            
            userDefaults.set("mai", forKey: "time")
            perform(#selector(becomeController.mai), with: nil, afterDelay: 0.2)
            
        } else if (value == "mai") {
            
            perform(#selector(becomeController.mai), with: nil, afterDelay: 0.2)
            
        } else if (value == "già") {
            
            perform(#selector(becomeController.già), with: nil, afterDelay: 0.2)
            
        }
        
        
        
    }
    
    func mai () {
        performSegue(withIdentifier: "segueInfo", sender: nil)
    }
    
    func già () {
        performSegue(withIdentifier: "segueMain", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
