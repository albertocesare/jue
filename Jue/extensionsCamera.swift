//
//  extensionsCamera.swift
//  Jue
//
//  Created by Alberto Cesare Barbon on 08/04/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import AVFoundation

extension ViewController {

    
    @IBAction func swapping () {
        
        view.bringSubview(toFront: btnPick)
        captureSession.stopRunning()
        
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                print(input)
                captureSession.removeInput(input)
            }
        }
        
        switch a {
        case 0:
            
            
            a = 1
            swap(position: 1)
            captureSession.startRunning()
            
            break
        case 1:
            
            a = 0
            swap(position: 0)
            captureSession.startRunning()
            
            break
        default:
            break
        }
        
    }
    
    func swap(position: Int) {
        
        if let devices = AVCaptureDevice.devices() as? [AVCaptureDevice] {
            // Loop through all the capture devices on this phone
            for device in devices {
                // Make sure this particular device supports video
                if (device.hasMediaType(AVMediaTypeVideo)) {
                    // Finally check the position and confirm we've got the back camera
                    
                    switch position {
                    case 0:
                        
                        
                        if(device.position == AVCaptureDevicePosition.back) {
                            captureDevice = device
                            if captureDevice != nil {
                                print("Capture device found")
                                beginSession()
                            }
                        }
                        
                        break
                    case 1:
                        
                        
                        if(device.position == AVCaptureDevicePosition.front) {
                            captureDevice = device
                            if captureDevice != nil {
                                print("Capture device found")
                                beginSession()
                            }
                        }
                        
                        break
                    default:
                        break
                    }
                }
            }
        }
        
        view.bringSubview(toFront: hashtag)
        view.bringSubview(toFront: labelHash)
        view.bringSubview(toFront: buttonPic)
        view.bringSubview(toFront: btnPick)
        view.bringSubview(toFront: btSettings)
        view.bringSubview(toFront: btHeart)
    }
    
    
    func beginSession() {
        
        do {
            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
            stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
            
            if captureSession.canAddOutput(stillImageOutput) {
                captureSession.addOutput(stillImageOutput)
            }
            
        }
        catch {
            print("error: \(error.localizedDescription)")
        }
        
        guard let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) else {
            print("no preview layer")
            return
        }
        
        self.view.layer.addSublayer(previewLayer)
        previewLayer.frame = self.view.layer.frame
        captureSession.startRunning()
        
        //self.view.addSubview(navigationBar)
        self.view.addSubview(imgOverlay)
        self.view.addSubview(btnCapture)
    }
    
    func saveToCamera() {
        
        if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo) {
            
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (CMSampleBuffer, Error) in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(CMSampleBuffer) {
                    
                    if let cameraImage = UIImage(data: imageData) {
                        
                        self.foto = cameraImage
                        
                        //UIImageWriteToSavedPhotosAlbum(cameraImage, nil, nil, nil)
                    }
                }
            })
            
            
            
            
            
        }
        
        
        
        
        /*if let videoConnection = stillImageOutput.connection(withMediaType: AVMediaTypeVideo){
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (buffer:CMSampleBuffer!, error: NSError!) -> Void in
                
                
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
                    let imageCamera = UIImage(data: imageData!)
                    self.foto = imageCamera!
                
                
                } as! (CMSampleBuffer?, Error?) -> Void)
        }*/
        
    }
    
    func pic () {
        centesimiDisecondo += Int(timer.timeInterval * 10)
        
        if(centesimiDisecondo >= 10) {
            secondi = secondi + 1
            //millisecondi = 0
        }
        progress.isHidden = false
        progress.value = CGFloat(centesimiDisecondo)
    }
    
    func video () {
        
    }

}
