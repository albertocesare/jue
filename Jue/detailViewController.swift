//
//  detailViewController.swift
//  Jue
//
//  Created by Alberto Cesare Barbon on 08/04/17.
//  Copyright © 2017 Alberto Cesare Barbon. All rights reserved.
//

import UIKit
import Photos
import UIView_draggable

class detailViewController: UIViewController, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var labelHash: UILabel!
    @IBOutlet weak var btShare: UIButton!
    @IBOutlet weak var btDismiss: UIButton!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var btSave : UIButton!
    @IBOutlet weak var blur : UIVisualEffectView!
    
    var nuovaFoto: UIImage!
    var hasht: String!
    
    var documentController: UIDocumentInteractionController!
    
    var frame : CGRect!

    @IBOutlet weak var imageview: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imageview.image = nuovaFoto
        labelHash.text = hasht
        labelHash.adjustsFontSizeToFitWidth = true
        viewLabel.frame = frame
        
        viewLabel.enableDragging()
        viewLabel.shouldMoveAlongX = false
        viewLabel.shouldMoveAlongY = true
        viewLabel.cagingArea = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func share() {
        
        btDismiss.isHidden = true
        btShare.isHidden = true
        //blur.isHidden = true
        btSave.isHidden = true
        
        shareToSocial(image: captureScreen()!)
        
        btDismiss.isHidden = false
        btShare.isHidden = false
        //blur.isHidden = false
        btSave.isHidden = false
        
    }
    
    @IBAction func dis() {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func saveToLibrary () {
        
        btDismiss.isHidden = true
        btShare.isHidden = true
        btSave.isHidden = true
        
        sleep(UInt32(0.1))
        
        let imageRepresentation = UIImagePNGRepresentation(captureScreen()!)
        let imageData = UIImage(data: imageRepresentation!)
        UIImageWriteToSavedPhotosAlbum(imageData!, nil, nil, nil)
        
        let imageGree = UIImage(named: "save.png")?.maskWith(color: UIColor.init(red: 140/255, green: 193/255, blue: 82/255, alpha: 1))
        btSave.setImage(imageGree, for: .normal)
        
        
        btDismiss.isHidden = false
        btShare.isHidden = false
        btSave.isHidden = false
        
    }
    
    func shareToSocial(image: UIImage) {
        
        /*let instagramURL = NSURL(string: "instagram://app")
        
        if (UIApplication.shared.canOpenURL(instagramURL! as URL)) {
            
            let imageData = UIImageJPEGRepresentation(nuovaFoto, 100)
            
            let captionString = "caption"
            
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
            
            /*if imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic) == false {
                
                return
                
            } else {
                let fileURL = NSURL(fileURLWithPath: writePath)
                
                self.documentController = UIDocumentInteractionController(url: fileURL as URL)
                
                self.documentController.delegate = self
                
                self.documentController.uti = "com.instagram.exlusivegram"
                
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                
            }*/
            
            do {
                
                try imageData?.write(to: URL(fileURLWithPath: writePath), options: .atomic)
                
                let fileURL = NSURL(fileURLWithPath: writePath)
                
                self.documentController = UIDocumentInteractionController(url: fileURL as URL)
                
                self.documentController.delegate = self
                
                self.documentController.uti = "com.instagram.exlusivegram"
                
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                
            } catch let error {
                print(error)
            }
            
            
        } else {
            print(" Instagram isn't installed ")
        }*/
        
        // If you want to put an image
        let imag : UIImage = image
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [imag], applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityType.postToWeibo,
            UIActivityType.print,
            UIActivityType.assignToContact,
            UIActivityType.saveToCameraRoll,
            UIActivityType.addToReadingList,
            UIActivityType.postToFlickr,
            UIActivityType.postToVimeo,
            UIActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UIImage {
    
    func maskWith(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return newImage
    }        
}
